/obj/item/clothing/under/suit/captain_blacksuit
	name = "captains black suit"
	desc = "A very sleek naval captain's uniform for those who think they're commanding a battleship.."
	icon = 'modular_skyrat/icons/obj/clothing/uniform.dmi'
	mob_overlay_icon = 'modular_skyrat/icons/mob/clothing/uniform.dmi'
	icon_state = "captainblacksuit"
	item_state = "captainblacksuit"

/obj/item/clothing/suit/armor/hos/carrier
	name = "chief enforcer's carrier set"
	desc = "A robust, kevlar plate carrier with an attached set of arm and leg guards."
	icon = 'modular_skyrat/icons/obj/clothing/enforcer.dmi'
	mob_overlay_icon = 'modular_skyrat/icons/mob/clothing/enforcer.dmi'
	icon_state = "cecarrier"
	item_state = "cecarrier"

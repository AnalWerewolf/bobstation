//Israel gun
/obj/item/gun/ballistic/automatic/mini_uzi
	icon = 'modular_skyrat/icons/obj/bobstation/guns/smg.dmi'
	righthand_file = 'modular_skyrat/icons/obj/bobstation/guns/inhands/smg_righthand.dmi'
	lefthand_file = 'modular_skyrat/icons/obj/bobstation/guns/inhands/smg_lefthand.dmi'
	icon_state = "uzi"
	item_state = "uzi"
	generic_magazine_overlays = FALSE

//Saber should be an MP5 that would be cool i think
/obj/item/gun/ballistic/automatic/proto
	icon = 'modular_skyrat/icons/obj/bobstation/guns/40x32.dmi'
	icon_state = "mp5"
	item_state = "arg"
	desc = "A prototype three-round burst 9mm submachine gun, designated 'SABR'. Looks eerily similar to another submachine gun..."
	can_suppress = FALSE

//C20r
/obj/item/gun/ballistic/automatic/c20r
	icon = 'modular_skyrat/icons/obj/bobstation/guns/smg.dmi'
	righthand_file = 'modular_skyrat/icons/obj/bobstation/guns/inhands/smg_righthand.dmi'
	lefthand_file = 'modular_skyrat/icons/obj/bobstation/guns/inhands/smg_lefthand.dmi'
	icon_state = "c20r"
	item_state = "c20r"
	generic_magazine_overlays = FALSE

//WT550 augh
/obj/item/gun/ballistic/automatic/wt550
	name = "security semi-auto WT-550"
	desc = "An outdated personal defence weapon. Uses 4.6x30mm rounds."
	icon = 'modular_skyrat/icons/obj/bobstation/guns/smg.dmi'
	icon_state = "wt550"
	item_state = "arg"

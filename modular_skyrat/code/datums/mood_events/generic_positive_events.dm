//Masochist mood
/datum/mood_event/paingood
	description = "<span class='nicegreen'>Pain cleanses the mind and the soul.</span>\n"
	mood_change = 4
	timeout = 2 MINUTES

//Hydration
/datum/mood_event/wellhydrated
	description = "<span class='nicegreen'>I'm gonna burst!</span>\n"
	mood_change = 4

/datum/mood_event/hydrated
	description = "<span class='nicegreen'>I have recently had some water.</span>\n"
	mood_change = 2

//Cremation
/datum/mood_event/cremated
	description = "<span class='nicegreen'>Not everyone can be saved. At least they have been put to rest.</span>\n"
	mood_change = 4
	timeout = 15 MINUTES
